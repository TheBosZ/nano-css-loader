const path = require('path');
const webpack = require('webpack');
const nano = require('./src/nano');

module.exports = {
	mode: 'development',
	plugins: [
		new webpack.optimize.ModuleConcatenationPlugin(),
	],
	entry: path.resolve(__dirname, './src/index.js'),
	devtool: false,
	output: {
		filename: 'main.min.js',
		path: path.resolve(__dirname, 'dist'),
	},
	module: {
		rules: [
			{
				test: /tyles\.js$/,
				loader: path.resolve(__dirname, '../dist/loader'),
				exclude: /node_modules/,
				options: {
					nano,
					outputFile: path.resolve(__dirname, 'dist/gen-styles.css'),
				},
			}
		],
	},
};
