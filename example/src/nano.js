const { create } = require('nano-css');

const nano = create();
require('nano-css/addon/rule').addon(nano);
require('nano-css/addon/sheet').addon(nano);
require('nano-css/addon/nesting').addon(nano);
require('nano-css/addon/prefixer').addon(nano);
require('nano-css/addon/keyframes').addon(nano);
require('nano-css/addon/virtual').addon(nano);

module.exports = nano;
