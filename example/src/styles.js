const nano = require('./nano');

const styles = {
	button: {
		cursor: 'default',
	},
};

module.exports = nano.sheet(styles);
