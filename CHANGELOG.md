# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.3 - 2019-05-23
- Recognize `.global()`

## 1.0.2 - 2019-05-07
- Fixed import problem

## 1.0.1 - 2019-05-07
- Initial release
