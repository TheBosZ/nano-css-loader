const MagicString = require('magic-string');
const { walk } = require('estree-walker');
const acorn = require('acorn');

function getReplacement(code, id) {
	let canRemoveImport = true;
	const magicString = new MagicString(code);
	const result = acorn.parse(code, { sourceType: 'module' });
	let importName = 'nano';
	let foundImport = false;
	let isNano = false;
	walk(result, {
		enter(node) {
			// Find the import declarations for nano and keep track of it
			const walkThat = this;
			if (!node || !node.type) {
				return;
			}

			if (node.type === 'ImportDeclaration' && node.specifiers && !foundImport) {
				const specs = node.specifiers;
				specs.forEach((spec) => {
					if ((spec.imported && spec.imported.name === 'nano') ||
						(spec.local && spec.local.name === 'nano')
					) {
						importName = spec.local.name;
						isNano = true;
						foundImport = true;
					}
				});
			}

			if (node.type === 'VariableDeclaration' && node.declarations && node.declarations.length && !foundImport) {
				// Check to see if this declaration is the require('nano'); we're looking for
				const decs = node.declarations;
				decs.forEach((dec) => {
					if (dec.id) {
						// They might use const nano = require('./nano');
						if (
							dec.id.type === 'Identifier' &&
							dec.id.name === 'nano' &&
							!foundImport
						) {
							foundImport = true;
							importName = dec.id.name;
							isNano = true;
							foundImport = true;
						}

						// They've might use: const { nano } = require('./nano');
						if (
							dec.id.type === 'ObjectPattern' &&
							dec.id.properties &&
							dec.id.properties.length &&
							!foundImport
						) {
							const props = dec.id.properties;
							props.forEach((prop) => {
								if (prop.key && prop.key.name === 'nano') {
									foundImport = true;
									importName = prop.key.name;
									isNano = true;
									foundImport = true;
								}
							});
						}
					}
				});
			}

			// Find any calls to nano.sheet
			if (node.type === 'CallExpression' && node.callee && node.callee.type === 'MemberExpression') {
				const { callee } = node;
				if (callee.object.name === importName) {
					// If the call was to something *other* than nano.sheet or nano.rule or nano.put or nano.global, we can't remove the import
					if (
						callee.property.name !== 'sheet' &&
						callee.property.name !== 'rule' &&
						callee.property.name !== 'put' &&
						callee.property.name !== 'global'
					) {
						canRemoveImport = false;
					}

					if (node.arguments) {
						const args = node.arguments;
						let x;
						if (args.length) {
							try {
								// eslint-disable-next-line import/no-dynamic-require
								x = require(id);
							} catch (ex) {
								console.warn('Failed to simplify nano call');
								console.warn(`Failed to load file: ${ex.toString()}`);
								canRemoveImport = false;
							}

							if (x && canRemoveImport) {
								try {
									const replacedString = x.default ? JSON.stringify(x.default) : JSON.stringify(x);
									if (callee.property.name === 'sheet' && replacedString.length < 3) {
										console.warn('Replacement string is not long enough, not going to overwrite');
										canRemoveImport = false;
									} else {
										magicString.addSourcemapLocation(0);
										magicString.addSourcemapLocation(code.length);
										magicString.overwrite(0, code.length, replacedString);
										// Finish going through the tree, we're done
										walkThat.skip();
									}
								} catch (ex) {
									console.warn(`Failed to simplify nano call: ${ex.toString()}`);
									canRemoveImport = false;
								}
							}
						}
					}
				}
			}
		},
	});

	if (isNano && canRemoveImport) {
		if (magicString.length() < 3) {
			return 'module.exports = 1;';
		}
		return `module.exports = ${magicString.toString()};`;
	}
	return null;
}

module.exports = getReplacement;
