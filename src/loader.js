const fs = require('fs');
const { getOptions }  = require('loader-utils');
const getReplacement = require('./getReplacement');

module.exports = function(source) {
	const options = getOptions(this);
	const result = getReplacement(source, this.resourcePath);
	if (result === null) {
		this.emitWarning(`Was not able to remove Nano from ${this.resourcePath}`);
		return source;
	}

	if (options.outputFile) {
		fs.writeFileSync(options.outputFile, options.nano.raw);
	}
	return result;
};
