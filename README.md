#Webpack Nano-CSS Plugin


A webpack plugin to extract styles from [Nano-CSS](https://github.com/streamich/nano-css/) and remove Nano from the bundle, if possible.

